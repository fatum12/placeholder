<?php
/**
 * Генератор картинок-заглушек
 */
define('ROOT', __DIR__);
define('MIN_WIDTH', 1);
define('MAX_WIDTH', 2000);
define('MIN_HEIGHT', 1);
define('MAX_HEIGHT', 2000);
define('DEFAULT_BACKGROUND_COLOR', 'ccc');

require('include/color.php');
require('include/image.php');

$query = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null);
$text = isset($_GET['text']) ? $_GET['text'] : null;

$query = trim($query, '/');
if (!$query) {
	echo file_get_contents(ROOT . '/help/index.html');
	exit;
}

if (strpos($query, '?') !== false) {
	$components = explode('?', $query);
	$query = $components[0];
}

// определяем параметры запроса
// /ШИРИНА[xВЫСОТА][/ЦВЕТ_ФОНА[/ЦВЕТ_ТЕКСТА]][.ФОРМАТ]
if (!preg_match('/^(\d+)(x(\d+))?(\/([0-9a-f]{3}|[0-9a-f]{6})(\/([0-9a-f]{3}|[0-9a-f]{6}))?)?(\.(jpe?g|png|gif))?$/i', $query, $matches)) {
	header('HTTP/1.0 404 Not Found');
	exit(1);
}

//print_r($matches);
// ширина
$width = (int) $matches[1];
if ($width < MIN_WIDTH) {
	$width = MIN_WIDTH;
}
elseif ($width > MAX_WIDTH) {
	$width = MAX_WIDTH;
}

// высота
if (isset($matches[3]) && $matches[3] != '') {
	$height = (int) $matches[3];
	if ($height < MIN_HEIGHT) {
		$height = MIN_HEIGHT;
	}
	elseif ($height > MAX_HEIGHT) {
		$height = MAX_HEIGHT;
	}
}
else {
	$height = $width;
}

// цвет фона
if (isset($matches[5]) && $matches[5] != '') {
	$backgroundColor = $matches[5];
}
else {
	$backgroundColor = DEFAULT_BACKGROUND_COLOR;
}
$backgroundColor = hex2rgb($backgroundColor);

// цвет текста
if (isset($matches[7]) && $matches[7] != '') {
	$textColor = hex2rgb($matches[7]);
}
else {
	$backgroundColorHsv = rgb2hsv($backgroundColor['r'], $backgroundColor['g'], $backgroundColor['b']);
	$contrastColor = getContrastColor($backgroundColorHsv['h'], $backgroundColorHsv['s'], $backgroundColorHsv['v']);
	$textColor = hsv2rgb($contrastColor['h'], $contrastColor['s'], $contrastColor['v']);
}

// формат изображения
if (isset($matches[9]) && $matches[9] != '') {
	$format = strtolower($matches[9]);
}
else {
	$format = 'png';
}

// текст
if ($text !== null) {
	$text = str_replace('+', ' ', $text);
	$text = str_replace('|', "\n", $text);
}
else {
	$text = $width . 'x' . $height;
}

createImage($width, $height, $backgroundColor, $textColor, $text, $format);