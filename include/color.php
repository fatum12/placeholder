<?php
/**
 * @param string $hex Цвет в формате HEX (например, ffffff)
 * @return array Массив RGB компонентов
 */
function hex2rgb($hex) {
	$color = strtolower($hex);
	if (strlen($color) == 3) {
		$tmpColor = '';
		for($i = 0; $i < 3; $i++) {
			$tmpColor .= $color[$i] . $color[$i];
		}
		$color = $tmpColor;
	}
	$result = array();
	$result['r'] = hexdec(substr($color, 0, 2));
	$result['g'] = hexdec(substr($color, 2, 2));
	$result['b'] = hexdec(substr($color, 4, 2));

	return $result;
}

/**
 * @param integer $dR 0-255
 * @param integer $dG 0-255
 * @param integer $dB 0-255
 * @return string
 */
function rgb2hex($dR, $dG, $dB) {
	$color = dechex( ($dR << 16) + ($dG << 8) + $dB );
	return str_repeat('0', 6 - strlen($color)) . $color;
}

/**
 * @param integer $r Красная составляющая цвета 0-255
 * @param integer $g Зеленая составляющая цвета 0-255
 * @param integer $b Синяя составляющая цвета 0-255
 * @return array Массив HSV компонентов (0-360, 0-100, 0-100)
 */
function rgb2hsv($r, $g, $b) {
	$max = max($r, $g, $b);
	$min = min($r, $g, $b);

	$hsv = array(
		'v' => $max / 2.55,
		's' => (!$max) ? 0 : (1 - ($min / $max)) * 100,
		'h' => 0,
	);
	$dmax = $max - $min;

	if (!$dmax) {
		return $hsv;
	}

	if ($max == $r) {
		if ($g < $b) {
			$hsv['h'] = ($g - $b) * 60;

		}
		elseif ($g == $b) {
			$hsv['h'] = 360;
		}
		else {
			$hsv['h'] = ((($g - $b) / $dmax) * 60) + 360;
		}

	}
	elseif ($max == $g) {
		$hsv['h'] = ((($b - $r) / $dmax) * 60) + 120;
	}
	else {
		$hsv['h'] = ((($r - $g) / $dmax) * 60) + 240;
	}

	return $hsv;
}

/**
 * @param integer $iH 0-360
 * @param integer $iS 0-100
 * @param integer $iV 0-100
 * @return array
 */
function hsv2rgb($iH, $iS, $iV) {
	$H = $iH / 360.0;
	$S = $iS / 100.0;
	$V = $iV / 100.0;
	//1
	$H *= 6;
	//2
	$I = floor($H);
	$F = $H - $I;
	//3
	$M = $V * (1 - $S);
	$N = $V * (1 - $S * $F);
	$K = $V * (1 - $S * (1 - $F));
	//4
	switch ($I) {
		case 0:
			list($R, $G, $B) = array($V, $K, $M);
			break;
		case 1:
			list($R, $G, $B) = array($N, $V, $M);
			break;
		case 2:
			list($R, $G, $B) = array($M, $V, $K);
			break;
		case 3:
			list($R, $G, $B) = array($M, $N, $V);
			break;
		case 4:
			list($R, $G, $B) = array($K, $M, $V);
			break;
		case 5:
		case 6: //for when $H=1 is given
			list($R, $G, $B) = array($V, $M, $N);
			break;
	}

	return array(
		'r' => floor($R * 255),
		'g' => floor($G * 255),
		'b' => floor($B * 255),
	);
}

function getContrastColor($hue, $saturation, $brightness) {
    // Rotate Hue by 180 degrees and correct the possible overlow.
    $hue = $hue + 180;
    if ($hue > 360) {
		$hue -= 360;
    }

    // Analyse the saturation - if it is low, then modify brightness.
    if ($saturation < 20) {
		$brightness = $brightness < 50 ? 100 : 0;
    }

    // Set saturation to maximum value.
	$saturation = 100;

    return array(
		'h' => $hue,
		's' => $saturation,
		'v' => $brightness,
	);
}