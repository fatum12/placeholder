<?php

function createImage($width, $height, $backgroundColor, $textColor, $text, $ext) {
	$img = imagecreatetruecolor($width, $height);
	$gdBackColor = imagecolorallocate($img, $backgroundColor['r'], $backgroundColor['g'], $backgroundColor['b']);
	imagefill($img, 0, 0, $gdBackColor);

	$font = ROOT . '/fonts/ptserif_regular.ttf';
	$gdTextColor = imagecolorallocate($img, $textColor['r'], $textColor['g'], $textColor['b']);

	$lines = explode("\n", $text);
	$maxLength = getLongestSubstringLength($text);
	$fontSize = max(min($width / $maxLength * 0.8, $height / count($lines) * 0.4), 4);
	$textSizes = getTextSizes($text, $font, $fontSize);
	$textPosX = ceil(($width - $textSizes['width']) / 2) + $textSizes['left'];
	$textPosY = ceil(($height - $textSizes['height']) / 2) + $textSizes['top'];
	imagettftext($img, $fontSize, 0, $textPosX, $textPosY, $gdTextColor, $font, $text);

	$offset = 30 * 24 * 3600; // 30 дней
	header("Cache-control: public");
	header("Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT");

	switch($ext) {
		case 'gif':
			header('Content-Type: image/gif');
			imagegif($img);
			break;
		case 'jpg':
		case 'jpeg':
			header('Content-Type: image/jpeg');
			imagejpeg($img);
			break;
		default:
			header('Content-type: image/png');
			imagepng($img);
	}
	imagedestroy($img);
}

function getTextSizes($text, $fontFile, $fontSize) {
	$rect = imagettfbbox($fontSize, 0, $fontFile, $text);

	$minX = min(array($rect[0],$rect[2],$rect[4],$rect[6]));
	$maxX = max(array($rect[0],$rect[2],$rect[4],$rect[6]));
	$minY = min(array($rect[1],$rect[3],$rect[5],$rect[7]));
	$maxY = max(array($rect[1],$rect[3],$rect[5],$rect[7]));

	return array(
		"left"   => abs($minX) - 1,
		"top"    => abs($minY) - 1,
		"width"  => $maxX - $minX,
		"height" => $maxY - $minY,
		"box"    => $rect
	);
}

function getLongestSubstringLength($text) {
	$strings = explode("\n", $text);
	$longest = 0;
	foreach ($strings as $string) {
		if (strlen($string) > $longest) {
			$longest = strlen($string);
		}
	}
	return $longest;
}
